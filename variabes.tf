variable "region" {
  type        = string
  default     = "eu-west-1"
  description = "AWS region"
}
variable "front_s3_bucket_name" {
  type        = string
  default     = "front-complete-project-deployment"
  description = "Name of s3 front-end bucket"
}
variable "application_name" {
  type        = string
  default     = "complete_project_deployment"
  description = "Name of the application"
}
variable "application_description" {
  description = "Sample application based on Elastic Beanstalk & Docker"
}
variable "application_environment" {
  type        = string
  default     = "production"
  description = "Name of application environment"
}

terraform {
  backend "s3" {
    region               = "eu-west-1"
    encrypt              = true
    bucket               = "complete-project-deployment-terraform-state"
    key                  = "tfstate"
    workspace_key_prefix = "prod"
  }
}
provider "aws" {
  region = "eu-west-1"
}

resource "aws_ecr_repository" "ecr_repository" {
  name                 = "backend"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}
